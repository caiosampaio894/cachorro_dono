from flask import Blueprint, request, current_app, jsonify
from app.models.dono_model import DonoModel

bp=Blueprint('dono', __name__)

@bp.route('/donos', methods=['POST'])
def create():
    data = request.get_json()
    new_dono = DonoModel(
        nome=data['nome'],
        sobrenome=data['sobrenome'],
        idade=data['idade']
    )

    session = current_app.db.session

    session.add(new_dono)
    session.commit()

    return jsonify(new_dono)