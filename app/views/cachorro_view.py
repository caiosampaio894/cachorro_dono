from warnings import resetwarnings
from flask import Blueprint, json, request, current_app, jsonify
from app.models.cachorro_model import CachorroModel

bp=Blueprint('cachorro', __name__)

@bp.route('/cachorros', methods=['POST'])
def create():
    data = request.get_json()
    new_dog = CachorroModel(
        nome=data['nome'],
        raca=data['raca'],
        idade=data['idade']
    )

    session = current_app.db.session

    session.add(new_dog)
    session.commit()

    return jsonify(new_dog), 201

@bp.route('/cachorros', methods=['GET'])
def get_dogs():
    get_all_dogs = CachorroModel.query.all()
    
    return jsonify(get_all_dogs), 200

@bp.route('/cachorros/<int:id>', methods=['GET'])
def get_dog_from_id(id: int):
    get_dog_from_id = CachorroModel.query.get(id)

    return jsonify(get_dog_from_id), 200

@bp.route('/cachorros/<int:id>', methods=['DELETE'])
def delete_dog_from_id(id:int):
    delete_dog_from_id = CachorroModel.query.get_or_404(id)
    session = current_app.db.session
    session.delete(delete_dog_from_id)
    session.commit()

    return jsonify(delete_dog_from_id), 200

# @bp.route('/cachorros/deleted', methods=['DELETE'])
# def delete_all_dogs():
#     delete_all_dogs = CachorroModel.query.get_or_404()
#     session = current_app.db.session
#     session.delete(delete_all_dogs)
#     session.commit()

#     return jsonify(delete_all_dogs), 200