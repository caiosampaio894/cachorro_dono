from sqlalchemy import Column, Integer, String
from dataclasses import dataclass

from app.configs.database import db

@dataclass
class DonoModel(db.Model):
    id: int
    nome: str
    sobrenome: str
    idade: int

    __tablename__ = 'dono'

    id = Column(Integer, primary_key=True)
    nome = Column(String, nullable=False)
    sobrenome = Column(String, nullable=False)
    idade = Column(Integer)